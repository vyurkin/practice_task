import 'package:flutter/material.dart';
import 'package:flutter_application_1/models/user.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class SaveButton extends StatelessWidget {
  //Map<String, String> saveStateForm;
  final GlobalKey<FormState> key;
  final String? typeProperty;
  final TextEditingController? controller, secondContoller;

  SaveButton(
      {required this.typeProperty,
      required this.key,
      required this.controller,
      @required this.secondContoller});

  //final user = Provider.of<UserState>(context).;

  @override
  Widget build(BuildContext context) {
    void onPress() {
      if (this.typeProperty == 'phone') {
        BlocProvider.of<UserBloc>(context)
            .mapEventToState(UpdatePhone(controller.toString()));
      } else if (this.typeProperty == 'email') {
        BlocProvider.of<UserBloc>(context)
            .mapEventToState(UpdateEmail(controller.toString()));
      } else if (this.typeProperty == 'name' || this.typeProperty == 'sName') {
        BlocProvider.of<UserBloc>(context)
            .mapEventToState(UpdateFirstName(controller.toString()));
        BlocProvider.of<UserBloc>(context)
            .mapEventToState(UpdateSName(controller.toString()));
      } else
        print('TYPEPROPERTY ERROR');
    }

    return GestureDetector(
      onTap: () {
        onPress();
        //print('save button');
      },
      child: Material(
        clipBehavior: Clip.antiAlias,
        shape: BeveledRectangleBorder(
            borderRadius: BorderRadius.only(topRight: Radius.circular(20))),
        child: Container(
          color: Color(0xffCEFC4C),
          width: 327,
          height: 56,
          alignment: Alignment.center,
          child: Text(
            'Сохранить',
            style: TextStyle(
              color: Colors.black,
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      ),
    );
  }
}
