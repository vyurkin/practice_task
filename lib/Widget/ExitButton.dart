import 'package:flutter/material.dart';

import '../constants.dart';

class ExitButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: () {
          print('exit');
        },
        child: Text(
          'Выйти',
          style: TextStyle(
              color: textButtonColor,
              fontSize: 16,
              fontWeight: FontWeight.w500),
        ));
  }
}
