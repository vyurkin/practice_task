import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../constants.dart';

class MyCustomButton extends StatelessWidget {
  final IconData? leftIcon;
  final String? labelIcon;
  final IconData? rightIcon;
  final Widget? route;
  final double? fontSize;

  MyCustomButton(
      {@required this.leftIcon,
      @required this.labelIcon,
      @required this.rightIcon,
      @required this.route,
      @required this.fontSize});

  bool _checkLeftIcon(IconData? icon) {
    if (icon != null) {
      return true;
    } else
      return false;
  }

  @override
  Widget build(BuildContext context) {
    double width = _checkLeftIcon(this.leftIcon) ? 18.0 : 0.0;
    return TextButton(
      onPressed: () {
        route != null
            ? Navigator.push(
                context, CupertinoPageRoute(builder: (contex) => route!))
            : print('Custom button');
      },
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Icon(
            leftIcon,
            color: iconColor,
            size: 26,
          ),
          SizedBox(
            width: width,
          ),
          Expanded(
            child: Text(
              labelIcon!,
              style: TextStyle(
                color: textButtonColor,
                fontSize: this.fontSize,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Icon(rightIcon, color: iconColor, size: 26)
        ],
      ),
    );
  }
}

class defaultPage extends StatelessWidget {
  const defaultPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Center(
        child: Text('ОШИБКА'),
      ),
    ));
  }
}
