import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/models/user.dart';
import 'package:provider/provider.dart';
import 'package:image_picker/image_picker.dart';

import '../constants.dart';

class CustomAvatar extends StatefulWidget {
  final bool? editProfile;

  CustomAvatar({Key? key, required this.editProfile}) : super(key: key);

  @override
  _CustomAvatarState createState() => _CustomAvatarState(this.editProfile);
}

class _CustomAvatarState extends State<CustomAvatar> {
  final bool? editProfile;
  _CustomAvatarState(this.editProfile);

  late File imageFile;

  final picker = ImagePicker();

  Future getImagefromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      imageFile = File(pickedFile!.path);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserState>(
      builder: (context, user, child) => editProfile == false
          ? Container(
              alignment: Alignment.center,
              width: 128,
              height: 128,
              decoration: BoxDecoration(
                  color: backgroundColorAvatar, shape: BoxShape.circle),
              child: urlAvavar == ''
                  ? Text(
                      '${context.select((UserState p) => p.firstName[0])}',
                      style: TextStyle(
                          color: textColorAvatar,
                          fontSize: 64,
                          fontWeight: FontWeight.w500),
                    )
                  : Container(
                      width: 128,
                      height: 128,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(urlAvavar))),
                    ),
            )
          : GestureDetector(
              onTap: () {
                //getImagefromGallery();
                print('d image');
              },
              child: Stack(alignment: Alignment.center, children: <Widget>[
                Container(
                  width: 128,
                  height: 128,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          fit: BoxFit.cover, image: NetworkImage(urlAvavar))),
                ),
                Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.black45,
                  ),
                  width: 128,
                  height: 128,
                  alignment: Alignment.center,
                  child: Icon(
                    Icons.add_a_photo_outlined,
                    color: Colors.white60,
                    size: 26,
                  ),
                ),
              ]),
            ),
    );
  }
}
