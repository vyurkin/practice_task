import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/constants.dart';
import 'package:flutter_application_1/pages/HomePage.dart';
import 'package:flutter_application_1/pages/MyStarts.dart';
import 'package:flutter_application_1/pages/profile_pages/ProfilePage.dart';

class BottomNavigator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.directions_run),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.group,
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
            )
          ],
          activeColor: Colors.white,
          inactiveColor: iconColor,
          backgroundColor: backgroudColorBottomNavigation,
        ),
        tabBuilder: (context, index) {
          switch (index) {
            case 0:
              return CupertinoTabView(builder: (context) {
                return CupertinoPageScaffold(child: HomePage());
              });
            case 1:
              return CupertinoTabView(builder: (context) {
                return CupertinoPageScaffold(
                  child: HomePage(),
                );
              });
            case 2:
              return CupertinoTabView(builder: (context) {
                return CupertinoPageScaffold(child: HomePage());
              });
            case 3:
              return CupertinoTabView(builder: (context) {
                return CupertinoPageScaffold(child: ProfilePage());
              });
            default:
              return CupertinoTabView(builder: (context) {
                return CupertinoPageScaffold(child: HomePage());
              });
          }
        });
  }
}

class Page2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: OutlinedButton(
      child: Text('Page_2'),
      onPressed: () {
        Navigator.pushNamed(context, '/page_2');
      },
    )));
  }
}

class Page3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Text(
        'Pag3',
        style: TextStyle(color: Colors.black),
      ),
    ));
  }
}

class Page2_2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Page_2'),
      ),
    );
  }
}

class Page4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Text(
        'Pag4',
        style: TextStyle(color: Colors.black),
      ),
    ));
  }
}
