import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../constants.dart';

class CustomBackButton extends StatelessWidget {
  final Widget? route;

  CustomBackButton({@required this.route});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        route != null
            ? Navigator.pop(
                context, CupertinoPageRoute(builder: (contex) => route!))
            : print('BACK');
      },
      icon: Icon(
        Icons.arrow_back,
        size: 26,
        color: Colors.white,
      ),
    );
  }
}
