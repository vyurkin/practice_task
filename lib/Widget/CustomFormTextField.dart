import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomFormTextField extends StatefulWidget {
  final String? typeValidate;
  final TextInputType keyboadrd;
  final TextEditingController? textContoller;
  final int lengthLimiit;

  CustomFormTextField(
      {Key? key,
      this.typeValidate,
      required this.keyboadrd,
      required this.textContoller,
      required this.lengthLimiit})
      : super(key: key);

  @override
  _CustomFormTextFieldState createState() => _CustomFormTextFieldState(
      this.typeValidate,
      this.keyboadrd,
      this.textContoller!,
      this.lengthLimiit);
}

class _CustomFormTextFieldState extends State<CustomFormTextField> {
  final String? typeValidate;
  final TextInputType keyboadrd;
  final TextEditingController textContoller;
  final int lenghtLimit;

  _CustomFormTextFieldState(
      this.typeValidate, this.keyboadrd, this.textContoller, this.lenghtLimit);

  String _validate(String? value) {
    if (this.typeValidate == 'phone') {
      return _phoneValidate(value!)!;
    } else if (this.typeValidate == 'email') {
      return _emailValidate(value!)!;
    } else {
      return _textValidate(value!)!;
    }
  }

  String? _phoneValidate(String value) {
    final _phoneExp = RegExp(r'[^0-9]+-');
    if (value.isEmpty) {
      return 'Это поле не может быть пустым';
    } else if (!_phoneExp.hasMatch(value)) {
      return 'Номер телефона не может содержать символы алфавита';
    } else
      return null;
  }

  String? _emailValidate(String value) {
    String pattern =
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?)*$";
    final _emailExp = RegExp(pattern);
    if (value.isEmpty) {
      return 'Это поле не может быть пустым';
    } else if (!_emailExp.hasMatch(value)) {
      return 'Введите корректную почту';
    } else
      return null;
  }

  String? _textValidate(String value) {
    final _phoneExp = RegExp(r'^[a-zA-z]+$');
    if (value.isEmpty) {
      return 'Это поле не может быть пустым';
    } else if (!_phoneExp.hasMatch(value)) {
      return 'Это поле может состоять только из символов';
    } else
      return null;
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: textContoller,
      inputFormatters: [LengthLimitingTextInputFormatter(lenghtLimit)],
      keyboardType: keyboadrd,
      validator: _validate,
      style: TextStyle(
          color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.w500),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(10.0),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(0)),
            borderSide: BorderSide(color: Colors.white54)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(0)),
            borderSide: BorderSide(color: Colors.white54)),
      ),
    );
  }
}
