import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/models/user.dart';
import 'package:flutter_application_1/pages/MyStarts.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'Widget/BottomNavigator.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //debugShowCheckedModeBanner: false,
      home: MultiProvider(providers: [
        ChangeNotifierProvider<UserState>(
          create: (_) => UserState(
              'Владимир', 'Путин', '+7 999 888-77-66', 'test@gmailc.com'),
        ),
        BlocProvider(
          create: (_) => UserBloc(),
        ),
        FutureProvider<MyStartList>(
            create: (BuildContext context) async =>
                MyStartsProvider().loadMyStart(),
            initialData: MyStartList([])),
      ], child: BottomNavigator()),
    );
  }
}
