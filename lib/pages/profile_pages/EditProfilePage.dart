import 'package:flutter/material.dart';

import 'package:flutter_application_1/Widget/Avatar.dart';
import 'package:flutter_application_1/Widget/BackButton.dart';
import 'package:flutter_application_1/Widget/CustomButton.dart';
import 'package:flutter_application_1/constants.dart';
import 'package:flutter_application_1/models/user.dart';
import 'package:flutter_application_1/pages/profile_pages/ProfilePage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import 'EditDataPage.dart';
import 'EditEmailPage.dart';
import 'EditPhonePage.dart';

class JosKeys {
  static final keyName = GlobalKey<FormState>();
  static final keyEmail = GlobalKey<FormState>();
  static final keyPhone = GlobalKey<FormState>();
}

class EditProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      left: false,
      right: false,
      child: Scaffold(
        body: _EditBuildProfilePage(),
        backgroundColor: mainBackgroudColor,
      ),
    );
  }
}

class _EditBuildProfilePage extends StatefulWidget {
  @override
  __EditBuildProfilePageState createState() => __EditBuildProfilePageState();
}

class __EditBuildProfilePageState extends State<_EditBuildProfilePage> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserStates>(
      builder: (_, state) => Stack(
        children: <Widget>[
          Positioned(
            child: CustomBackButton(
              route: ProfilePage(),
            ),
            top: (MediaQuery.of(context).size.width / 3 - 68),
            left: 26,
          ),
          Container(
            margin: EdgeInsets.only(top: 60, right: 124, left: 136),
            width: 128,
            height: 128,
            decoration: BoxDecoration(shape: BoxShape.circle),
            child: GestureDetector(
              onTap: () {
                print('DOWNLOAD IMAGE');
              },
              child: Stack(alignment: Alignment.center, children: <Widget>[
                Container(
                  child: CustomAvatar(
                    editProfile: true,
                  ),
                  width: 128,
                  height: 128,
                  decoration: BoxDecoration(shape: BoxShape.circle),
                ),
                Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.black45,
                  ),
                  width: 128,
                  height: 128,
                  alignment: Alignment.center,
                  child: Icon(
                    Icons.add_a_photo_outlined,
                    color: Colors.white60,
                    size: 26,
                  ),
                ),
              ]),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: 212,
              left: 40,
            ),
            width: 375,
            height: 43,
            // color: Colors.red,
            child: Text(
              '${state.firstName} ${state.sName}',
              textAlign: TextAlign.start,
              style: TextStyle(
                  fontSize: 36.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 263, left: 8, right: 24),
            //color: Colors.green,
            width: 200,
            height: 40,
            child: MyCustomButton(
              labelIcon: 'Редактировать',
              fontSize: 12.0,
              route: EditDataPage(),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: 320,
              left: 8,
              right: 24,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                MyCustomButton(
                  labelIcon: '${state.phone}',
                  rightIcon: Icons.keyboard_arrow_right,
                  route: EditPhonePage(),
                  fontSize: textSizeButton,
                ),
                MyCustomButton(
                  labelIcon: '${state.email}',
                  rightIcon: Icons.keyboard_arrow_right,
                  route: EditEmailPage(),
                  fontSize: textSizeButton,
                ),
                Divider(
                  height: 76,
                ),
                MyCustomButton(
                  labelIcon: 'Дата рождения',
                  rightIcon: Icons.keyboard_arrow_right,
                  fontSize: textSizeButton,
                ),
                MyCustomButton(
                  labelIcon: 'Пол',
                  rightIcon: Icons.keyboard_arrow_right,
                  fontSize: textSizeButton,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
