import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/Widget/Avatar.dart';
import 'package:flutter_application_1/Widget/BackButton.dart';
import 'package:flutter_application_1/Widget/CustomButton.dart';
import 'package:flutter_application_1/Widget/ExitButton.dart';
import 'package:flutter_application_1/constants.dart';
import 'package:flutter_application_1/models/user.dart';
import 'package:flutter_application_1/pages/FeedBackPage.dart';
import 'package:flutter_application_1/pages/MyStarts.dart';
import 'package:flutter_application_1/pages/PostsInTheCommunityPage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import 'EditProfilePage.dart';

//var testuser = testUser;

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      left: false,
      right: false,
      child: Scaffold(
        body: BuildProfilePage(),
        backgroundColor: mainBackgroudColor,
      ),
    );
  }
}

class BuildProfilePage extends StatefulWidget {
  @override
  _BuildProfilePageState createState() => _BuildProfilePageState();
}

class _BuildProfilePageState extends State<BuildProfilePage> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserStates>(
        builder: (_, state) => SafeArea(
              top: false,
              bottom: false,
              left: false,
              right: false,
              child: Stack(
                children: <Widget>[
                  Positioned(
                    child: CustomBackButton(),
                    top: 68,
                    left: 26,
                  ),
                  Positioned(
                    top: 60,
                    left: 127,
                    right: 128,
                    child: CustomAvatar(
                      editProfile: false,
                    ),
                  ),
                  Positioned(
                    top: 212,
                    left: 50,
                    right: 24,
                    child: Text(
                      '${state.firstName} ${state.sName}',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontSize: 36.0,
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 26, right: 24),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SizedBox(
                          height: 280,
                        ),
                        Expanded(
                          flex: 1,
                          child: MyCustomButton(
                            leftIcon: Icons.person_outlined,
                            labelIcon: 'Личные данные',
                            rightIcon: Icons.keyboard_arrow_right,
                            route: EditProfilePage(),
                            fontSize: textSizeButton,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Expanded(
                          flex: 1,
                          child: MyCustomButton(
                            leftIcon: Icons.library_books,
                            labelIcon: 'Посты в сообществе',
                            rightIcon: Icons.keyboard_arrow_right,
                            route: PostsInTheCommunityPage(),
                            fontSize: textSizeButton,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Expanded(
                          flex: 1,
                          child: MyCustomButton(
                            leftIcon: Icons.directions_run_outlined,
                            labelIcon: 'Мои старты',
                            rightIcon: Icons.keyboard_arrow_right,
                            route: MyStartsPage(),
                            fontSize: textSizeButton,
                          ),
                        ),
                        SizedBox(
                          height: 174,
                        ),
                        Expanded(
                          flex: 1,
                          child: MyCustomButton(
                            leftIcon: Icons.messenger_outline_outlined,
                            labelIcon: 'Обратная связь',
                            rightIcon: Icons.keyboard_arrow_right,
                            route: FeedBackPage(),
                            fontSize: textSizeButton,
                          ),
                        ),
                        SizedBox(
                          height: 75,
                        ),
                        Expanded(flex: 1, child: ExitButton()),
                        SizedBox(
                          height: 40,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ));
  }
}
