import 'package:flutter/material.dart';
import 'package:flutter_application_1/Widget/BackButton.dart';
import 'package:flutter_application_1/Widget/CustomFormTextField.dart';
import 'package:flutter_application_1/Widget/SaveButton.dart';
import 'package:flutter_application_1/constants.dart';
import 'package:flutter_application_1/models/user.dart';
import 'package:flutter_application_1/pages/profile_pages/EditProfilePage.dart';
import 'package:flutter_application_1/pages/profile_pages/ProfilePage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class EditDataPage extends StatefulWidget {
  @override
  _EditDataPageState createState() => _EditDataPageState();
}

class _EditDataPageState extends State<EditDataPage> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserStates>(builder: (_, state) {
      final nameController = TextEditingController(text: state.firstName);
      final sNameController = TextEditingController(text: state.sName);
      return SafeArea(
        top: false,
        left: false,
        right: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: mainBackgroudColor,
          body: Stack(
            children: <Widget>[
              Positioned(
                child: CustomBackButton(
                  route: ProfilePage(),
                ),
                top: (MediaQuery.of(context).size.width / 3 - 61),
                left: 26,
              ),
              Positioned(
                  left: 70,
                  top: (MediaQuery.of(context).size.width / 3 - 53),
                  child: Text(
                    'Имя и фамилия',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.w500),
                  )),
              Container(
                margin: EdgeInsets.only(
                  top: 118,
                  left: 40,
                  right: 26,
                ),
                //color: Colors.white,
                child: Form(
                  key: JosKeys.keyName,
                  child: ListView(
                    children: <Widget>[
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        //color: Colors.red,
                        child: CustomFormTextField(
                            lengthLimiit: 16,
                            textContoller: nameController,
                            keyboadrd: TextInputType.text),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        //color: Colors.green,
                        child: CustomFormTextField(
                            lengthLimiit: 16,
                            textContoller: sNameController,
                            keyboadrd: TextInputType.text),
                      ),
                      Divider(
                        height: 400,
                      ),
                      SaveButton(
                        key: JosKeys.keyName,
                        typeProperty: 'name',
                        controller: nameController,
                        secondContoller: sNameController,
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}
