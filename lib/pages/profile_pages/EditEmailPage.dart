import 'package:flutter/material.dart';
import 'package:flutter_application_1/Widget/BackButton.dart';
import 'package:flutter_application_1/Widget/CustomFormTextField.dart';
import 'package:flutter_application_1/Widget/SaveButton.dart';
import 'package:flutter_application_1/constants.dart';
import 'package:flutter_application_1/models/user.dart';
import 'package:flutter_application_1/pages/profile_pages/EditProfilePage.dart';
import 'package:flutter_application_1/pages/profile_pages/ProfilePage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class EditEmailPage extends StatefulWidget {
  @override
  _EditEmailPageState createState() => _EditEmailPageState();
}

class _EditEmailPageState extends State<EditEmailPage> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserStates>(builder: (_, state) {
      final emailController = TextEditingController(text: state.email);
      return SafeArea(
        top: false,
        left: false,
        right: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: mainBackgroudColor,
          body: Stack(
            children: <Widget>[
              Positioned(
                child: CustomBackButton(
                  route: ProfilePage(),
                ),
                top: (MediaQuery.of(context).size.width / 3 - 61),
                left: 26,
              ),
              Positioned(
                  left: 70,
                  top: (MediaQuery.of(context).size.width / 3 - 53),
                  child: Text(
                    'E-mail',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.w500),
                  )),
              Container(
                margin: EdgeInsets.only(
                  top: 118,
                  left: 40,
                  right: 26,
                ),
                //color: Colors.white,
                child: Form(
                  key: JosKeys.keyEmail,
                  child: ListView(
                    children: <Widget>[
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        //color: Colors.red,
                        child: CustomFormTextField(
                          lengthLimiit: 32,
                          textContoller: emailController,
                          keyboadrd: TextInputType.emailAddress,
                        ),
                      ),
                      Divider(
                        height: 475,
                      ),
                      SaveButton(
                        key: JosKeys.keyEmail,
                        typeProperty: 'email',
                        controller: emailController,
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}
