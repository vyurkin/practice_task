import 'package:flutter/material.dart';
import 'package:flutter_application_1/Widget/BackButton.dart';
import 'package:flutter_application_1/Widget/CustomFormTextField.dart';
import 'package:flutter_application_1/Widget/SaveButton.dart';
import 'package:flutter_application_1/constants.dart';
import 'package:flutter_application_1/models/user.dart';
import 'package:flutter_application_1/pages/profile_pages/ProfilePage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import 'EditProfilePage.dart';

class EditPhonePage extends StatefulWidget {
  @override
  _EditPhonePageState createState() => _EditPhonePageState();
}

class _EditPhonePageState extends State<EditPhonePage> {
  final keyPhone = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserStates>(builder: (_, state) {
      final phoneController = TextEditingController(text: state.email);
      return SafeArea(
        top: false,
        left: false,
        right: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: mainBackgroudColor,
          body: Stack(
            children: <Widget>[
              Positioned(
                child: CustomBackButton(
                  route: ProfilePage(),
                ),
                top: (MediaQuery.of(context).size.width / 3 - 61),
                left: 26,
              ),
              Positioned(
                  left: 70,
                  top: (MediaQuery.of(context).size.width / 3 - 53),
                  child: Text(
                    'Телефон',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.w500),
                  )),
              Container(
                //color: Colors.red,
                margin: EdgeInsets.only(
                  top: 118,
                  left: 40,
                  right: 26,
                ),
                //color: Colors.white,
                child: Form(
                  key: JosKeys.keyPhone,
                  child: ListView(
                    children: <Widget>[
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        //color: Colors.red,
                        child: CustomFormTextField(
                          lengthLimiit: 16,
                          textContoller: phoneController,
                          keyboadrd: TextInputType.phone,
                        ),
                      ),
                      Divider(
                        height: 475,
                      ),
                      SaveButton(
                        key: JosKeys.keyPhone,
                        typeProperty: 'phone',
                        controller: phoneController,
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}
