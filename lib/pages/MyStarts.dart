import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/Widget/BackButton.dart';
import 'package:flutter_application_1/constants.dart';
import 'package:flutter_application_1/pages/profile_pages/ProfilePage.dart';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'dart:async';

import 'package:provider/provider.dart';

class MyStartsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //List<MyStartList> starts = Provider.of<List<MyStartList>>(context);
    //return Consumer(builder: (context, List<MyStartList> starts, _) {
    return SafeArea(
      top: false,
      bottom: false,
      left: false,
      right: false,
      child: Scaffold(
        backgroundColor: mainBackgroudColor,
        body: Stack(
          children: <Widget>[
            Positioned(
              child: CustomBackButton(
                route: ProfilePage(),
              ),
              top: 60,
              left: 26,
            ),
            Positioned(
              child: IconButton(
                  onPressed: () {
                    print('add');
                  },
                  icon: Icon(Icons.add, color: Colors.white, size: 26)),
              top: 60,
              right: 24,
              left: 335,
            ),
            Positioned(
                top: 118,
                left: 36,
                right: 26,
                child: Text(
                  'Мои старты',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: 36.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.white),
                )),
            Container(
              width: 327,
              margin: EdgeInsets.only(top: 175, left: 36, right: 26),
              child: Starts(),
            ),
          ],
        ),
      ),
    );
    // });
  }
}

class Starts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<MyStartList>(
      builder: (context, startList, child) => ListView.separated(
        padding: EdgeInsets.only(top: 10),
        separatorBuilder: (context, int index) => Divider(
          height: 56,
        ),
        itemCount: startList.items.length,
        itemBuilder: (context, index) {
          return Container(
            width: 327,
            height: 343,
            //color: Colors.red,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  '${startList.items[index].title}',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                      fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.place, color: Colors.white,),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      '1 марта',
                      style: TextStyle(color: Colors.grey, fontSize: 14.0),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'суздаль',
                      style: TextStyle(color: Colors.grey, fontSize: 14.0),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: <Widget>[
                    Text(
                      '1457',
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                    SizedBox(
                      width: 35,
                    ),
                    Text(
                      '14 км',
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                    SizedBox(
                      width: 35,
                    ),
                    Text(
                      '01:02:30',
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        'Cтартовый номер',
                        style: TextStyle(color: Colors.grey, fontSize: 12.0),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        'Дистанция',
                        style: TextStyle(color: Colors.grey, fontSize: 12.0),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(
                        'Время',
                        style: TextStyle(color: Colors.grey, fontSize: 12.0),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  width: 327,
                  height: 160,
                  //color: Colors.green,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        width: 327,
                        height: 160,
                        child: Image(
                            fit: BoxFit.fill,
                            image: NetworkImage(
                                'https://in-w.ru/wp-content/uploads/2017/01/Begun.jpg')),
                      ),
                      GestureDetector(
                        onTap: () {
                          print('IMAGE START');
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              top: 88, left: 16, right: 16, bottom: 16),
                          width: 295,
                          height: 56,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border: Border.all(color: Colors.white)),
                          child: Text(
                            'Смотреть все фото',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14.0,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}

class MyStart {
  final String title, date, place, startNumber, distance, time;
  final List<String> images;

  const MyStart(this.title, this.date, this.place, this.startNumber,
      this.distance, this.time, this.images);

  MyStart.fromJson(Map<String, dynamic> json)
      : this.title = json['title'],
        this.date = json['date'],
        this.place = json['place'],
        this.startNumber = json['startNumber'],
        this.distance = json['distance'],
        this.time = json['time'],
        this.images =
            (json['images'] as List).map((image) => image as String).toList();
}

class MyStartList {
  final List<MyStart> items;
  MyStartList(this.items);

  MyStartList.fromJson(List<dynamic> myStartsJson)
      : this.items =
            myStartsJson.map((myStart) => MyStart.fromJson(myStart)).toList();
}

class MyStartsProvider {
  final String _dataPath = "assets/MyStarts.json";
  late MyStartList myStarts;

  Future<String> loadAsset() async {
    return await Future.delayed(Duration(seconds: 1), () async {
      return await rootBundle.loadString(_dataPath);
    });
  }

  Future<MyStartList> loadMyStart() async {
    var dataString = await loadAsset();
    Map<String, dynamic> jsonMyStartData = jsonDecode(dataString);
    myStarts = MyStartList.fromJson(jsonMyStartData['MyStarts']);
    print('done loading starts');
    return myStarts;
  }
}
