import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/Widget/CustomButton.dart';
import 'package:flutter_application_1/constants.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      left: false,
      right: false,
      child: Scaffold(
        backgroundColor: mainBackgroudColor,
        body: Center(
          child: Text('PAGE 1'),
        ),
      ),
    );
  }
}
