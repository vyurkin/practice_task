import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserState extends ChangeNotifier {
  String firstName, sName, phone, email;
  UserState(this.firstName, this.sName, this.phone, this.email);

  void updateFirstName(String value) {
    firstName = value;
    notifyListeners();
  }

  void updateSname(String value) {
    sName = value;
    notifyListeners();
  }

  void updatePhone(String value) {
    phone = value;
    notifyListeners();
  }

  void updateEmail(String value) {
    email = value;
    notifyListeners();
  }
}

class User {
  String firstName, sName, phone, email;

  User(this.firstName, this.sName, this.phone, this.email);
}

abstract class UserEvent {
  const UserEvent();
}

class UpdateFirstName extends UserEvent {
  final String firstName;

  UpdateFirstName(this.firstName);
}

class UpdateSName extends UserEvent {
  final String sName;

  UpdateSName(this.sName);
}

class UpdatePhone extends UserEvent {
  final String phone;

  UpdatePhone(this.phone);
}

class UpdateEmail extends UserEvent {
  final String email;

  UpdateEmail(this.email);
}

class UserStates {
  final String? firstName, sName, phone, email;

  UserStates(
      {@required this.firstName,
      @required this.sName,
      @required this.phone,
      @required this.email});
}

class FirstNameAdded extends UserStates {
  final String? firstName;

  FirstNameAdded({required this.firstName}) : super(firstName: firstName);
}

class SNameAdded extends UserStates {
  final String? sName;

  SNameAdded({required this.sName}) : super(sName: sName);
}

class PhoneAdded extends UserStates {
  final String? phone;

  PhoneAdded({required this.phone}) : super(phone: phone);
}

class EmailAdded extends UserStates {
  final String? email;

  EmailAdded({required this.email}) : super(email: email);
}

class UserBloc extends Bloc<UserEvent, UserStates> {
  UserBloc()
      : super(UserStates(
            firstName: 'Владимир',
            sName: 'Путин',
            phone: '+7 999 888-77-66',
            email: 'test@gmailc.com'));

  late String _firstName, _sName, _phone, _email;

  //@override
  //UserStates get initialState = UserStates('Владимир', 'Путин', '+7 999 888-77-66', 'test@gmailc.com');

  @override
  Stream<UserStates> mapEventToState(UserEvent event) async* {
    if (event is UpdateFirstName) {
      _firstName = event.firstName;
      yield FirstNameAdded(firstName: _firstName);
    } else if (event is UpdateSName) {
      _sName = event.sName;
      yield SNameAdded(sName: _sName);
    } else if (event is UpdateEmail) {
      _email = event.email;
      yield EmailAdded(email: _email);
    } else if (event is UpdatePhone) {
      _phone = event.phone;
      yield PhoneAdded(phone: _phone);
    }
  }
}
