import 'package:flutter/material.dart';

const mainBackgroudColor = Color(0xff232327);
const backgroudColorBottomNavigation = Color(0xff383841);
const textButtonColor = Colors.white;
const iconColor = Color(0xff95959D);
const textSizeButton = 16.0;
const textSizeName = 34.0;
const sizeIconButton = 14;
const backgroundColorAvatar = Color(0xff383841);
const textColorAvatar = Color(0xff232327);

const urlAvavar =
    'https://wl-adme.cf.tsp.li/resize/728x/jpg/828/489/b2756c5cdd8b6216f063d69448.jpg';
const emptyUrl = '';
